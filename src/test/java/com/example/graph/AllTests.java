package com.example.graph;

import com.example.graph.elements.EdgeTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        EdgeTest.class,
        GraphTest.class
})
public class AllTests {
}
