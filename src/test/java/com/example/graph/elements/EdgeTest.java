package com.example.graph.elements;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class EdgeTest {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testSettingNull() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Value cannot be null");
        Edge unEdge = new UndirectedEdge(null, null);
        //Edge diEdge = new DirectedEdge(null, null);
    }

    @Test
    public void testEquals() {
        Vertex a = new Vertex("A");
        Vertex b = new Vertex("B");

        Edge unAB = new UndirectedEdge(a, b);
        Edge unBA = new UndirectedEdge(b, a);
        Assert.assertEquals(unAB, unBA);

        Edge diAB = new DirectedEdge(a, b);
        Edge diBA = new DirectedEdge(b, a);
        Assert.assertNotEquals(diAB, diBA);

        Assert.assertNotEquals(unAB, diAB);
        Assert.assertNotEquals(unAB, diBA);
        Assert.assertNotEquals(unBA, diBA);
        Assert.assertNotEquals(unBA, diAB);
    }

    @Test
    public void testToString() {
        Vertex a = new Vertex("A");
        Vertex b = new Vertex("B");

        Edge unAB = new UndirectedEdge(a, b);
        System.out.println(unAB);
        Assert.assertEquals("Edge{Vertex{A} -- Vertex{B}, 1.0}", unAB.toString());

        Edge diAB = new DirectedEdge(a, b);
        System.out.println(diAB);
        Assert.assertEquals("Edge{Vertex{A} -> Vertex{B}, 1.0}", diAB.toString());
    }
}
