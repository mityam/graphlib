package com.example.graph.elements;

import com.example.util.Util;

public abstract class Edge {
    protected Vertex tail;
    protected Vertex head;
    protected double weight = 1.0;

    public Edge(Vertex tail, Vertex head) {
        setTail(tail);
        setHead(head);
    }

    public Edge(Vertex tail, Vertex head, double weight) {
        setTail(tail);
        setHead(head);
        setWeight(weight);
    }

    public Vertex getTail() {
        return tail;
    }

    public void setTail(Vertex tail) {
        Util.nullCheck(tail);
        this.tail = tail;
    }

    public Vertex getHead() {
        return head;
    }

    public void setHead(Vertex head) {
        Util.nullCheck(head);
        this.head = head;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
