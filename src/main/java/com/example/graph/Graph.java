package com.example.graph;

import com.example.graph.elements.Edge;
import com.example.graph.elements.Vertex;
import com.example.util.dto.Pair;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Consumer;

public abstract class Graph {
    //Non thread safe.
    //protected Map<Vertex, List<Edge>> adjacencyList = new HashMap<>();

    //Simple thread safe. For current implementation mb enough, but overall needed more complex solution.
    protected Map<Vertex, List<Edge>> adjacencyList = new ConcurrentHashMap<>();

    public void addVertex(Vertex v) {
        //adjacencyList.putIfAbsent(v, new ArrayList<>());
        adjacencyList.putIfAbsent(v, new CopyOnWriteArrayList<>());
    }

    public Vertex[] getVertices() {
        return adjacencyList.keySet().toArray(new Vertex[0]);
    }

    public abstract void addEdge(Vertex tail, Vertex head);

    public void BFS(Vertex start, Consumer<Vertex> fun) {
        if (!adjacencyList.isEmpty() && fun != null) {
            if (start != null && !adjacencyList.containsKey(start)) {
                return;
            }
            List<Vertex> visited = new ArrayList<>();
            Queue<Vertex> queue = new ArrayDeque<>();

            Vertex current = getMapObject(start);
            if (current == null) {
                current = adjacencyList.keySet().toArray(new Vertex[0])[0];
            }
            visited.add(current);
            queue.add(current);
            while ((current = queue.poll()) != null) {
                List<Edge> edges = adjacencyList.remove(current);
                fun.accept(current);
                adjacencyList.put(current, edges);
                for (Edge edge : edges) {
                    Vertex next = edge.getHead();
                    if (!visited.contains(next)) {
                        visited.add(next);
                        queue.add(next);
                    }
                }
            }
        }
    }

    private Vertex getMapObject(Vertex key) {
        if (key != null) {
            for (Vertex v : adjacencyList.keySet()) {
                if (key.equals(v)) {
                    return v;
                }
            }
        }
        return null;
    }

    public void changeAllVertices(Consumer<Vertex> fun) {
        if (!adjacencyList.isEmpty() && fun != null) {
            for (Vertex v : adjacencyList.keySet().toArray(new Vertex[0])) {
                List<Edge> edges = adjacencyList.remove(v);
                fun.accept(v);
                adjacencyList.put(v, edges);
            }
        }
    }

    public List<Edge> getPathDFS(Vertex from, Vertex to) {
        List<Edge> path = new ArrayList<>();
        if (adjacencyList.containsKey(from) && adjacencyList.containsKey(to)) {
            List<Vertex> visited = new ArrayList<>();
            DFS(from, to, visited, path);
        }
        return path;
    }

    private boolean DFS(Vertex current, Vertex to, List<Vertex> visited, List<Edge> path) {
        if (current.equals(to)) {
            return true;
        }
        visited.add(current);

        List<Edge> edges = adjacencyList.get(current);
        for (Edge edge : edges) {
            Vertex next = edge.getHead();
            if (!visited.contains(next)) {
                path.add(edge);
                boolean found = DFS(next, to, visited, path);
                if (found) {
                    return true;
                } else {
                    path.remove(edge);
                }
            }
        }
        return false;
    }

    public List<Edge> getPathIterativeDFS(Vertex from, Vertex to) {
        if (adjacencyList.containsKey(from) && adjacencyList.containsKey(to)) {
            List<Vertex> visited = new ArrayList<>();
            Stack<Pair<Vertex, List<Edge>>> stack = new Stack<>();
            stack.push(new Pair<>(from, new ArrayList<>()));
            while (!stack.empty()) {
                Pair<Vertex, List<Edge>> pair = stack.pop();
                Vertex current = pair.getFirst();
                if (current.equals(to)) {
                    return pair.getSecond();
                }
                visited.add(current);

                List<Edge> edges = adjacencyList.get(current);
                for (Edge edge : edges) {
                    Vertex next = edge.getHead();
                    if (!visited.contains(next)) {
                        List<Edge> path = new ArrayList<>(pair.getSecond());
                        path.add(edge);
                        stack.add(new Pair<>(next, path));
                    }
                }
            }
        }

        return new ArrayList<>();
    }
}
