package com.example.graph;

import com.example.graph.elements.Edge;
import com.example.graph.elements.Vertex;
import com.example.util.TestUtil;
import com.example.util.dto.Pair;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;

@RunWith(Enclosed.class)
public class GraphTest {
    /*
        Graph for testing:
            A -- B -- C
            \     \  /
             \     \/
              D -- E -- F   G
     */
    private static Graph createUndigraph() {
        Vertex a = new Vertex("A");
        Vertex b = new Vertex("B");
        Vertex c = new Vertex("C");
        Vertex d = new Vertex("D");
        Vertex e = new Vertex("E");
        Vertex f = new Vertex("F");
        Vertex g = new Vertex("G");
        List<Vertex> vertices = Arrays.asList(a, b, c, d, e, f, g);

        Graph graph = new Undigraph();
        for (Vertex vertex : vertices) {
            graph.addVertex(vertex);
        }

        graph.addEdge(a, b);
        graph.addEdge(a, d);
        graph.addEdge(b, c);
        graph.addEdge(b, e);
        graph.addEdge(c, e);
        graph.addEdge(d, e);
        graph.addEdge(e, f);

        return graph;
    }

    /*
        Digraph for testing:
            A -> B <- C -> H -> I
            ^     \   ^
             \     v /
              D <- E -> F <-G
     */
    private static Graph createDigraph() {
        Vertex a = new Vertex("A");
        Vertex b = new Vertex("B");
        Vertex c = new Vertex("C");
        Vertex d = new Vertex("D");
        Vertex e = new Vertex("E");
        Vertex f = new Vertex("F");
        Vertex g = new Vertex("G");
        Vertex h = new Vertex("H");
        Vertex i = new Vertex("I");
        List<Vertex> vertices = Arrays.asList(a, b, c, d, e, f, g, h, i);

        Graph graph = new Digraph();
        for (Vertex vertex : vertices) {
            graph.addVertex(vertex);
        }

        graph.addEdge(a, b);
        graph.addEdge(b, e);
        graph.addEdge(c, b);
        graph.addEdge(c, h);
        graph.addEdge(d, a);
        graph.addEdge(e, c);
        graph.addEdge(e, d);
        graph.addEdge(e, f);
        graph.addEdge(g, f);
        graph.addEdge(h, i);

        return graph;
    }

    @RunWith(Parameterized.class)
    public static class PathTest {
        private final Graph graph;
        private final Vertex from;
        private final Vertex to;

        public PathTest(Graph graph, String from, String to) {
            this.graph = graph;
            this.from = new Vertex(from);
            this.to = new Vertex(to);
        }

        @Test
        public void testDFS() {
            List<Edge> path = graph.getPathDFS(from, to);
            //System.out.println(Arrays.toString(path.toArray()));
            TestUtil.printPath(path);
            Assert.assertTrue(TestUtil.isCorrectPath(path));
        }

        @Test
        public void testIterativeDFS() {
            List<Edge> path = graph.getPathIterativeDFS(from, to);
            //System.out.println(Arrays.toString(path.toArray()));
            TestUtil.printPath(path);
            Assert.assertTrue(TestUtil.isCorrectPath(path));
        }

        @Parameterized.Parameters
        public static Collection<Object[]> data() {
            return Arrays.asList(new Object[][]{
                    {createUndigraph(), "A", "E"},
                    {createDigraph(), "A", "D"},
                    {createUndigraph(), "G", "E"},
                    {createDigraph(), "G", "D"},
                    {createUndigraph(), "Z", "E"},
                    {createDigraph(), "Z", "D"},
                    {createUndigraph(), "A", "G"},
                    {createDigraph(), "A", "G"},
                    {createUndigraph(), "A", "Z"},
                    {createDigraph(), "A", "Z"},
                    {new Undigraph(), "A", "D"},
                    {new Digraph(), "A", "D"}
            });
        }
    }

    @RunWith(Parameterized.class)
    public static class VariousPathTest {
        private final Graph graph;
        private final Vertex from;
        private final Vertex to;

        public VariousPathTest(Graph graph, Vertex from, Vertex to) {
            this.graph = graph;
            this.from = from;
            this.to = to;
        }

        @Test
        public void testDFS() {
            List<Edge> path = graph.getPathDFS(from, to);
            System.out.println("Vertices: " + from + " - " + to);
            TestUtil.printPath(path);
            Assert.assertTrue(TestUtil.isCorrectPath(path));
        }

        @Test
        public void testIterativeDFS() {
            List<Edge> path = graph.getPathIterativeDFS(from, to);
            System.out.println("Vertices: " + from + " - " + to);
            TestUtil.printPath(path);
            Assert.assertTrue(TestUtil.isCorrectPath(path));
        }

        @Parameterized.Parameters
        public static Collection<Object[]> data() {
            int graphsCount = 100;
            int pathsCount = 10;
            Pair<Integer, Integer> verticesRange = new Pair<>(20, 30);
            Pair<Integer, Integer> edgesRange = new Pair<>(20, 60);

            Random rnd = new Random();
            List<Object[]> data = new ArrayList<>();
            for (int i = 0; i < graphsCount; ++i) {
                Graph graph = TestUtil.generateGraph(
                        TestUtil.randomInt(verticesRange),
                        TestUtil.randomInt(edgesRange),
                        false);
                Vertex[] vertices = graph.getVertices();
                for (int j = 0; j < pathsCount; ++j) {
                    data.add(new Object[]{
                            graph,
                            vertices[rnd.nextInt(vertices.length)],
                            vertices[rnd.nextInt(vertices.length)]
                    });
                }
            }
            for (int i = 0; i < graphsCount; ++i) {
                Graph graph = TestUtil.generateGraph(
                        TestUtil.randomInt(verticesRange),
                        TestUtil.randomInt(edgesRange),
                        true);
                Vertex[] vertices = graph.getVertices();
                for (int j = 0; j < pathsCount; ++j) {
                    data.add(new Object[]{
                            graph,
                            vertices[rnd.nextInt(vertices.length)],
                            vertices[rnd.nextInt(vertices.length)]
                    });
                }
            }

            return data;
        }
    }

    @RunWith(Parameterized.class)
    public static class ConsumerTest {
        private final Supplier<Graph> createGraph;
        private Graph graph;

        public ConsumerTest(TestUtil.SupplierWrapper<Graph> wrapper) {
            this.createGraph = wrapper.getSupplier();
        }

        @Before
        public void init() {
            graph = createGraph.get();
        }

        @Test
        public void testBFS() {
            graph.BFS(null, v -> v.setLabel(v.getLabel() + "*"));
            graph.BFS(new Vertex("A*"), v -> v.setLabel("*" + v.getLabel()));

            Vertex a = new Vertex("*A*");
            graph.addVertex(a);
            System.out.println(Arrays.toString(graph.getVertices()));

            Vertex g = new Vertex("G");
            int aCount = 0;
            for (Vertex v : graph.getVertices()) {
                if (!g.equals(v)) {
                    String label = v.getLabel();
                    Assert.assertEquals(3, label.length());
                    Assert.assertTrue(label.startsWith("*"));
                    Assert.assertTrue(label.endsWith("*"));
                }
                if (a.equals(v)) {
                    ++aCount;
                }
            }
            Assert.assertEquals(1, aCount);
        }

        @Test
        public void testAll() {
            graph.changeAllVertices(v -> v.setLabel(v.getLabel() + "*"));
            graph.changeAllVertices(v -> v.setLabel("*" + v.getLabel()));

            Vertex a = new Vertex("*A*");
            graph.addVertex(a);
            System.out.println(Arrays.toString(graph.getVertices()));

            int aCount = 0;
            for (Vertex v : graph.getVertices()) {
                String label = v.getLabel();
                Assert.assertEquals(3, label.length());
                Assert.assertTrue(label.startsWith("*"));
                Assert.assertTrue(label.endsWith("*"));
                if (a.equals(v)) {
                    ++aCount;
                }
            }
            Assert.assertEquals(1, aCount);
        }

        @Test
        public void testNull() {
            Vertex[] before = graph.getVertices();
            graph.BFS(null, null);
            Vertex[] after = graph.getVertices();
            Assert.assertArrayEquals(before, after);

            graph.changeAllVertices(null);
            after = graph.getVertices();
            Assert.assertArrayEquals(before, after);
        }

        @Parameterized.Parameters
        public static Collection<Object[]> data() {
            return Arrays.asList(new Object[][]{
                    {new TestUtil.SupplierWrapper<>(GraphTest::createUndigraph)},
                    {new TestUtil.SupplierWrapper<>(GraphTest::createDigraph)}
            });
        }
    }
}
