package com.example.graph;

import com.example.graph.elements.DirectedEdge;
import com.example.graph.elements.Vertex;

public class Digraph extends Graph {
    @Override
    public void addEdge(Vertex tail, Vertex head) {
        adjacencyList.get(tail).add(new DirectedEdge(tail, head));
    }
}
