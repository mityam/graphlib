package com.example.util;

import com.example.graph.Digraph;
import com.example.graph.Graph;
import com.example.graph.Undigraph;
import com.example.graph.elements.Edge;
import com.example.graph.elements.Vertex;
import com.example.util.dto.Pair;

import java.util.List;
import java.util.Random;
import java.util.function.Supplier;

public class TestUtil {
    public static void printPath(List<Edge> path) {
        if (!path.isEmpty()) {
            StringBuilder pathStr = new StringBuilder("PATH: ");
            for (int i = 0; i < path.size(); ++i) {
                if (i == 0) {
                    pathStr.append(path.get(i).getTail().getLabel());
                }
                pathStr.append(" -> ").append(path.get(i).getHead().getLabel());
            }
            System.out.println(pathStr);
        }
    }

    public static Graph generateGraph(int verticesCount, int edgesCount, boolean directed) {
        Graph graph;
        if (directed) {
            graph = new Digraph();
        } else {
            graph = new Undigraph();
        }

        int left = 65; // letter 'A'
        int right = 90; // letter 'Z'
        int range = right - left + 1;

        Vertex[] vertices = new Vertex[verticesCount];
        for (int i = 0; i < verticesCount; ++i) {
            char ch = (char) (left + i % range);
            StringBuilder label = new StringBuilder();
            for (int j = 0; j < i /range + 1; ++j) {
                label.append(ch);
            }
            vertices[i] = new Vertex(label.toString());
            graph.addVertex(vertices[i]);
        }

        Random rnd = new Random();
        for (int i = 0; i < edgesCount; ++i) {
            Vertex v1 = vertices[rnd.nextInt(vertices.length)];
            Vertex v2 = vertices[rnd.nextInt(vertices.length)];
            graph.addEdge(v1, v2);
        }
        return graph;
    }

    public static boolean isCorrectPath(List<Edge> path) {
        if (!path.isEmpty()) {
            for (int i = 0; i < path.size() - 1; ++i) {
                if (!path.get(i).getHead().equals(path.get(i + 1).getTail())) {
                    return false;
                }
            }
        }
        return true;
    }

    public static int randomInt(Pair<Integer, Integer> range) {
        return (int) (Math.random() * (range.getSecond() - range.getFirst() + 1)) + range.getFirst();
    }

    public static class SupplierWrapper<T> {
        private final Supplier<T> supplier;

        public SupplierWrapper(Supplier<T> supplier) {
            this.supplier = supplier;
        }

        public Supplier<T> getSupplier() {
            return supplier;
        }
    }
}
