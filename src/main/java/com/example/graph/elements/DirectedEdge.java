package com.example.graph.elements;

import java.util.Objects;

public class DirectedEdge extends Edge {
    public DirectedEdge(Vertex tail, Vertex head) {
        super(tail, head);
    }

    public DirectedEdge(Vertex tail, Vertex head, double weight) {
        super(tail, head, weight);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Edge edge = (Edge) o;
        return Double.compare(edge.weight, weight) == 0 &&
                tail.equals(edge.tail) &&
                head.equals(edge.head);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tail, head, weight);
    }

    @Override
    public String toString() {
        return "Edge{" + tail + " -> " + head + ", " + weight + '}';
    }
}
