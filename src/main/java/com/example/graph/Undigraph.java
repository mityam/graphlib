package com.example.graph;

import com.example.graph.elements.UndirectedEdge;
import com.example.graph.elements.Vertex;

public class Undigraph extends Graph {
    @Override
    public void addEdge(Vertex tail, Vertex head) {
        adjacencyList.get(tail).add(new UndirectedEdge(tail, head));
        adjacencyList.get(head).add(new UndirectedEdge(head, tail));
    }
}
