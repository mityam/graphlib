package com.example.util;

public class Util {
    public static void nullCheck(Object value) {
        if (value == null) {
            throw new IllegalArgumentException("Value cannot be null");
        }
    }
}
